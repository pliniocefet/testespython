"""
    Escreva um programa que leia dois numeros.
    Imprima a divisão inteira do primeiro pelo segundo, assim como o resto da divisão.
    Utilize apenas os operadores de soma e subtração para calcular o resultado.
    Lembre-se de que podemos entender o quociente da divisão de dois numeros como a quantidade de vezes que podemos
    retirar o divisor do dividendo.
    Logo, 20 / 4 = 5, uma vez que podemos subtrair o numero 4 cinco vezes de 20
"""

dividendo = int(input("Digite o dividendo "))
divisor = int(input("Digite o divisor "))

x = 0
resto = dividendo

while x <= resto:
    resto = resto - divisor
    x = x + 1

print("O Resultado da divisão é %d " % x)
print("O resto da divisão é %d " % resto)
