"""
    Escreva um programa que leia 3 numeros e que imprima o maior e o menor
"""

n1 = int(input("Digite o primeiro numero "))
n2 = int(input("Digite o segundo numero "))
n3 = int(input("Digite o terceiro numero "))

maior = n1

if n2 > n3 and n2 > n1:
    maior = n2

if n3 > n2 and n3 > n1:
    maior = n3

menor = n1

if n2 < n3 and n2 < n1:
    menor = n2

if n3 < n2 and n3 < n1:
    menor = n3

print("O maior numero é {}" .format(maior))
print("O menor numero é {}" .format(menor))

