"""
    Apresente os 10 primeiros multiplos de 3 utilizando while
"""

print("Multiplos de 3")

x = 2

while x <= 30:
    if x % 3 == 0:
        print(x)
        x = x + 1
    else:
        x = x + 1
