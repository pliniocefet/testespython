"""
    Escreva um programa que pergunte a quantidade de km percorridos por um carro alugado pelo usuario,
    assim como a quantidade de dias pelos quais o carro foi alugado.
    Calcule o preço a pagar sabendo que o carro custa R$ 60,00 por dia e R$ 0,15 por km rodado.
"""

km = float(input("Digite a quantidade de km percorridos "))
dias = int(input("Digite a quantidade de dias utilizados "))

total = (dias * 60) + (km * 0.15)

print("Voce pagara um total de R$%.2f pelo aluguel do carro" % total)
