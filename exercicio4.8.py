"""
    Escreva um programa que leia 2 numeros e que pergunte qual operação você deseja realizar.
    Você deve poder calcular a soma(+), subtração(-), multiplicação(*) e divisão(/).
    Exiba o resultado  da operação solicitada
"""
numero1 = float(input("Digite o primeiro numero "))
numero2 = float(input("Digite o segundo numero "))
resultado = 0

print("Escolha uma das operações:")
opcao = int(input("1 - soma\n2 - subtração\n3 - multiplicação\n4 - divisão\n"))

if opcao == 1:
    resultado = numero1 + numero2
    print("O resultado da soma é {}" .format(resultado))
elif opcao == 2:
    resultado = numero1 - numero2
    print("O resultado da subtração é {}".format(resultado))
elif opcao == 3:
    resultado = numero1 * numero2
    print("O resultado da multiplicação é {}".format(resultado))
elif opcao == 4:
    resultado = numero1 / numero2
    print("O resultado da divisão é {}".format(resultado))
else:
    print("opção inválida")
