"""
    Escreva um programa que verifique se um número é palindromo.
    um número é palindromo se continua o mesmo caso seus digitos sejam invertidos.
    Exemplo 454, 10501
"""
numero = input("Digite um numero para saber se é palindromo ")
tamanho = len(numero) - 1
i = 0

while tamanho > i and numero[i] == numero[tamanho]:
    tamanho = tamanho - 1
    i = i + 1
if numero[i] == numero[tamanho]:
    print("%s é palindromo " % numero)
else:
    print("%s não é palindromo " % numero)


