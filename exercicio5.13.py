"""
    Escreva um programa que pergunte o valor inicial de uma dívida e o juro mensal
    Pergunte também o valor mensal que será pago
    Imprima o numero de meses para que a divida seja paga, o total pago e o total de juros pago

        ESTUDAR MAIS ESTES EXERCÍCIOS
"""

divida = float(input("Qual o valor da divida? "))
taxa = float(input("Qual a taxa de juros mensal? "))
pagamento = float(input("Quanto vai pagar por mes? "))
mes = 1

if (divida * (taxa / 100)) > pagamento:
    print("Voce nunca quitará a divida")
else:
    saldo = divida
    juros_pago = 0

    while saldo > pagamento:
        juros = saldo * (taxa / 100)
        saldo = saldo + juros - pagamento
        juros_pago = juros_pago + juros
        print("Saldo da divida no mes %d é de %6.2f" % (mes, saldo))
        mes = mes + 1

    print("Para pagar uma divida de R$%8.2f, a uma taxa de %5.2f%% de juros " % (divida, taxa))
    print("Voce precisará de %d meses, pagando um total de R$%8.2f de juros " % ((mes-1), juros_pago))
    print("No ultimo mes, voce teria um saldo residual de R$%8.2f a pagar " % saldo)
