"""
    Escreva um programa que pergunte a velocidade do carro de um usuario.
    Caso ultrapasse 80km/h, exiba uma mensagem dizendo que o usuario foi multado.
    Nesse casso, exiba o valor da multa, cobrando R$ 5,00 por km acima de 80.
"""

velocidade = float(input("Qual a velocidade do veículo? "))

if velocidade > 80:
    multa = (velocidade - 80) * 5.00
    print("Voce foi multado por excesso de velocidade. Valor da Multa R$ %.2f reais" % multa)
