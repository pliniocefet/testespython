"""
    Escreva um programa que leia um número e verifique se é ou não um número primo.
    Para fazer essa verificação, calcule o resto da divisão do número por 2 e depois por todos os números ímpares
    até o número lido.
    Se o resto de uma dessas divisões for igual a zero, o número não é primo.
    Observe que 0 e 1 não são primos e que 2 é o único número primo que é par.
"""

n = int(input("Digite um número para saber se é primo "))

if n < 0:
    print("Digite numeros positivos")
else:
    if n >= 1:
        print("2")
        p = 1
        y = 3

    while p < n:
        x = 3
        while x < y:
            if y % x == 0:
                break
            x = x + 2

        if x == y:
            print(x)
            p = p + 1
        y = y + 2
