"""
    Faça um programa que solicite o preço de uma mercadoria e o percentual de desconto
    Exiba o valor do desconto e o preço a pagar
"""

mercadoria = float(input("Digite o valor da mercadoria "))
desconto = float(input("Digite a porcentagem do desconto "))

valor_desconto = mercadoria * (desconto/100)
novo_valor = mercadoria - valor_desconto

print("O valor do desconto é R$ %.2f e o valor da mercadoria com desconto é R$ %.2f" % (valor_desconto, novo_valor))
