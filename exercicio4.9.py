"""
    Escreva um programa para aprovar o emprestimo bancario para compra de uma casa.
    O programa deve perguntar o valor da casa a comprar, o salario e a quantidade de anos a pagar.
    O valor da prestação mensal não pode ser superior a 30% do salario.
    Calcule o valor da prestação como sendo o valor da casa a comprar dividido pelo numero de meses a pagar
"""

valor_casa = float(input("Digite o valor da casa a financiar: "))
salario = float(input("Digite seu salario: "))
anos = int(input("Digite a quantidade de anos: "))
parcela = anos * 12
prestacao = valor_casa / parcela

if prestacao > (salario * 0.3):
    print("Infelizmente não podemos fazer o financiamento. Seu salario não é suficiente")
else:
    print("Você pagará %d parcelas de R$%6.2f" % (parcela, prestacao))

