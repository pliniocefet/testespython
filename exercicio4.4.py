"""
    Escreva um programa que pergunte o salario do funcionário e calcule o valor do aumento.
    Para salários superiores a R$ 1.250,00, calcule um aumento de 10%
    Para os inferiores ou iguais de 15%
"""

salario = float(input("Digite seu salario: "))
valor = 0
if salario > 1250:
    valor = salario + (salario * 0.10)

if salario <= 1250:
    valor = salario + (salario * 0.15)

print("Novo salario de {}" .format(valor))
