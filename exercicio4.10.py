"""
    Escreva um programa que calcule o preço a pagar pelo fornecimento de energia elétrica.
    Pergunte a quantidade de kWh consumida e o tipo de instalação: R residências, I para indústrias e C para comercios.
    Calcule o preço a pagar de acordo com a tabela a seguir:
"""

quantidade_kwh = float(input("Informe o consume de energia (kWh) "))
tipo = str(input("Informe o tipo de instalação: \nR - Residencial\nC - Comercial\nI - Industrial\n"))

if tipo == "r" or tipo == "R":
    if quantidade_kwh <= 500:
        print("O preço a pagar será de R$%6.2f" % (quantidade_kwh * 0.40))
    elif quantidade_kwh > 500 and quantidade_kwh < 1000:
        print("O preço a pagar será de R$%6.2f" % (quantidade_kwh * 0.65))
elif tipo == "c" or tipo == "C":
    if quantidade_kwh <= 1000:
        print("O preço a pagar será de R$%6.2f" % (quantidade_kwh * 0.55))
    elif quantidade_kwh > 1000 and quantidade_kwh < 5000:
        print("O preço a pagar será de R$%6.2f" % (quantidade_kwh * 0.60))
elif tipo == "i" or tipo == "I":
    if quantidade_kwh <= 5000:
        print("O preço a pagar será de R$%6.2f" % (quantidade_kwh * 0.55))
    else:
        print("O preço a pagar será de R$%6.2f" % (quantidade_kwh * 0.60))
else:
    print("Tipo de instalação inválida!")
