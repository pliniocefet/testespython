"""
    Escreva um programa para calcular a redução do tempo de vida de um fumante
    Pergunte a quantidade de cigarros fumados por dia e quantos anos ele ja fumou
    Considere que um fumante perde 10 minutos de vida a cada cigarro,
    calcule quantos dias de vida um fumante perderá.
    Exiba o total em dias.
"""

qtd_cigarro = float(input("Quantos cigarros fuma por dia? "))
qtd_anos = float(input("Quantos anos ja fumou? "))
min_perdido = 10
total = (((qtd_cigarro * 365) * qtd_anos) * min_perdido) / 1440


print("Voce ja perdeu %.2f dias de vida" % total)
