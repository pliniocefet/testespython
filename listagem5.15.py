"""
    Listagem 5.15 - Impressão de tabuadas
    Repetição com incremento de duas variáveis
"""

tabuada = 1

while tabuada <= 10:
    numero = 1
    while numero <= 10:
        print("%d x %d = %d" % (tabuada, numero, (tabuada * numero)))
        numero = numero + 1
    tabuada = tabuada + 1
