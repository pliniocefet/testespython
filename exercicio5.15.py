"""
    Escreva um programa para controlar uma pequena maquina registradora.
    Voce deve solicitar ao usuario que digite o codigo do produto e a quantidade comprada
    Utilize a tabela de codigo abaixo para obter o preço de cada produto
    1 - 0,50
    2 - 1,00
    3 - 4,00
    5 - 7,00
    9 - 8,00

    Seu programa deve exibir o total das compras depois que o usuario digitar 0
    Qualquer outro codigo deve gerar a mensagem de erro. "Código inválido"
"""

preco = 0
total = 0

while True:
    codigo = int(input("Digite o codigo do produto: "))

    if codigo == 0:
        break

    quantidade = int(input("Digite a quantidade: "))

    if codigo == 1:
        preco = 0.50
        total = total + (quantidade * preco)

    elif codigo == 2:
        preco = 1.00
        total = total + (quantidade * preco)

    elif codigo == 3:
        preco = 4.00
        total = total + (quantidade * preco)

    elif codigo == 5:
        preco = 7.00
        total = total + (quantidade * preco)

    elif codigo == 9:
        preco = 8.00
        total = total + (quantidade * preco)

    elif codigo != 1 or codigo != 2 or codigo != 3 or codigo != 5 or codigo != 9:
        print("codigo inválido")

print("Total das compras foi de R$%6.2f" % total)

