"""
    Faça um programa que calcule o aumento de um salário. Ele deve solicitar
    o valor do salario e a porcentagem de aumento. Exiba o valor do aumento
    Exiba o valor do aumento e o novo salario.
"""

salario = float(input("Digite o valor do salario: "))
aumento = int(input("Digite a porcentagem de aumento: "))
novo_salario = salario + (salario * (aumento / 100))
print("O aumento foi de %d%%" % aumento)
print("O novo salario é R$ %.2f" % novo_salario)
