"""
    Listagem 5.13 - Interrompendo a repetição
"""

s = 0

while True:
    v = int(input("Digite um numero para somar ou zero para sair: "))
    if v == 0:
        break
    s = s + v
print(s)
