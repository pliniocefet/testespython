"""
    Exiba o inicio e o fim da tabuada digitada
"""
inicio = int(input("Digite um numero para iniciar a tabuada "))
fim = int(input("Digite um numero encerrar a tabuada "))
x = 0

while x <= fim:
    print("%d x %d = %d " % (inicio, x, (inicio * x)))
    x = x + 1
