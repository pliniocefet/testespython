"""
    Escreva um programa que calcule a raiz quadrada de um número.
    Utilize o método de Newton para obter um resultado aproximado
    Sendo n o numero a obter a raiz quadrada, considere a base b = 2
    Calcule p usando a formula p=(b+(b/n))/2
    Agora calcule o quadrado de p
    A cada passo faça b=p e recalcule p usando a formula apresentada
    Pare quando a diferença absoluta entre n e p for menor que 0,0001.
"""

n = float(input("Digite um número para encontrar sua raiz quadrada: "))

b = 2
p = 0
while abs(n - (b*b)) > 0.0001:
    p = (b + (n / b)) / 2
    b = p

print("A raiz quadrada de %d é aproximadamente %8.2f " % (n, p))
