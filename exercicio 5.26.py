"""
    Escreva um programa que calcule o resto da divisão inteira entre 2 numeros.
    Utilize apenas as operações de soma e subtração para calcular este resultado.
"""

dividendo = int(input("Digite o numero a ser dividido "))
divisor = int(input("Digite o numero pelo qual será divido "))
quociente = 0
x = dividendo
resto = 0


while x >= divisor:

    x = x - divisor
    quociente = quociente + 1

resto = x


print("O resultado é %d" % quociente)
print("O resto da divisão é %d" % resto)

