"""
    Escreva um programa que pergunte a distância que um passageiro deseja percorrer em km.
    Calcule o preço da passagem, cobrando R$ 0,50 por km para viagens de até 200km e R$ 0,45
    para viagens mais longas
"""

dist = float(input("Qual a distância que pretende percorrer? "))
preco = 0

if dist <= 200:
    preco = dist * 0.50
else:
    preco = dist * 0.45

print("O preço da passagem será R$ %.2f" % preco)
