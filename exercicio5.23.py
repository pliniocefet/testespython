"""
    Escreva um programa que leia um número e verifique se é ou não um número primo.
    Para fazer essa verificação, calcule o resto da divisão do número por 2 e depois por todos os números ímpares
    até o número lido.
    Se o resto de uma dessas divisões for igual a zero, o número não é primo.
    Observe que 0 e 1 não são primos e que 2 é o único número primo que é par.
"""

primo = int(input("Digite um número para saber se é primo "))

if primo < 0:
    print("Digite numeros positivos")

if primo == 0 or primo == 1:
    print("Este número não é primo")
else:
    if primo == 2:
        print("%d é primo" % primo)
    elif primo % 2 == 0:
        print("%d não é primo" % primo)
    else:
        x = 3
        while x < primo:
            if primo % x == 0:
                break
            x = x + 2

        if x == primo:
            print("%d é primo" % primo)
        else:
            print("%d não é primo" % primo)


