"""
    Escreva um programa que leia 2 numeros. Imprima o resultado da multiplicação do primeiro pelo segundo.
    Utilize apenas os operadores de soma e subtração para calcular o resultado.
    Lembre-se de que podemos entender a multiplicação de dois numeros como somas sucessivas de um deles.
    Assim 4 x 5 = 5 + 5 + 5 + 5
"""

fator1 = int(input("Digite o primeiro numero da multiplicação "))
fator2 = int(input("Digite o segundo numero da multiplicação "))

x = 1
resultado = fator2

while x < fator1:
    resultado = resultado + fator2
    x = x + 1

print(resultado)
