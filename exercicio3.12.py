"""
    Faça um programa que calcule o tempo de uma viagem de carro.
    Pergunte a distância a percorrer e a velocidade média esperada para a viagem.
"""

distancia = float(input("Digite a distancia a percorrer "))
velocidade = float(input("Digite a velocidade media "))
tempo = distancia / velocidade

print("O tempo necessário será de %1.f horas" % tempo)
