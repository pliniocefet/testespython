"""
    Escreva um programa que leia numeros inteiros do teclado.
    O programa deve ler os numeros até que o usuario digite 0
    No final da execução exiba a quantidade de numeros digitados
    assim como a soma e a média aritimética.
"""

soma = 0
cont = 0

while True:
    numero = int(input("Digite um numero inteiro "))
    if numero == 0:
        break
    soma = soma + numero
    cont = cont + 1

print("Quantidade de numeros digitados %d" % cont)
print("A soma dos numeros é %d " % soma)
print("A média dos numeros é %.2f " % (soma / cont))
