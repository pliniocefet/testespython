"""
    Escreva um programa que converta uma temperatura digitada em °C em °F
"""

c = float(input("Digite a temperatura em °C a ser convertida para °F "))
f = ((9 * c)/5)+32

print("O valor convertido será de %0.f°F" % f)
