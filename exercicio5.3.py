"""
    Escreva um programa para escrever a contagem regressiva do lançamento de um foguete.
    O programa deve imprimir 10,9,8....0 Fogo!
"""

x = 10
while x >= 1:
    print("%d..." % x)
    x = x - 1
print("Fogo!")
