"""
    Escreva um programa que pergunte o depósito inicial e a taxa de juros de uma poupança.
    Exiba os valores mês a mês para os 24 primeiros meses.
    Escreva o total ganho com juros no período.

        ESTUDAR MAIS ESTES EXERCÍCIOS
"""
mes = 1

deposito = float(input("Digite o deposito incial: "))
taxa = float(input("Digite a taxa de juros: "))
investimento = float(input("Investimento mensal "))
soma = 0


while mes <= 23:
    soma = soma + (deposito + (deposito * (taxa / 100)) + investimento)
    print("Valor acumulado em %d foi %6.2f" % (mes, soma))
    mes = mes + 1


print("Valor total em %d meses foi %6.2f" % (mes, soma))
print("O ganho obtido com os juros foi de %8.2f" % (soma - deposito))
