"""
    Escreva um programa que exiba uma lista de opções (menu):
    Adição, subtração, multiplicação, divisão e sair
    Imprima a tabuada da opção escolhida.
    Repita até que a opção saída seja escolhida
"""


while True:
    print("Digite uma das opções abaixo: ")
    print("1 - para Tabuada de adição")
    print("2 - para Tabuada de subtração")
    print("3 - para Tabuada de multiplicação")
    print("4 - para Tabuada de divisão")
    print("0 - para sair")
    opcao = int(input(""))

    # Opção Adição
    if opcao == 1:
        numero = 0
        tabuada = 1
        while tabuada <= 10:
            numero = 0
            print("\n")
            while numero <= 10:
                print("%d + %d = %d" % (tabuada, numero, (tabuada + numero)))
                numero = numero + 1
            tabuada = tabuada + 1

    # Opção Subtração
    if opcao == 2:
        numero = 0
        tabuada = 1
        while tabuada <= 10:
            numero = 0
            print("\n")
            while numero <= 10:
                print("%d - %d = %d" % (tabuada, numero, (tabuada - numero)))
                numero = numero + 1
            tabuada = tabuada + 1

    # Opção Multiplicação
    if opcao == 3:
        numero = 0
        tabuada = 1
        while tabuada <= 10:
            numero = 0
            print("\n")
            while numero <= 10:
                print("%d x %d = %d" % (tabuada, numero, (tabuada * numero)))
                numero = numero + 1
            tabuada = tabuada + 1

    # Opção Divisão
    if opcao == 4:
        numero = 1
        tabuada = 1
        while tabuada <= 10:
            numero = 1
            print("\n")
            while numero <= 10:
                print("%d / %d = %.2f" % (tabuada, numero, (tabuada / numero)))
                numero = numero + 1
            tabuada = tabuada + 1

    if opcao == 0:
        break
